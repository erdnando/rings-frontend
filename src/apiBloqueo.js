import axios from 'axios';

export default(servidor) => {
  
    return axios.create({
        baseURL: servidor + '/pdrservice/',
 
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    })
}