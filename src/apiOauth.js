import axios from 'axios';

export default(servidor) => {

    var user = 'client';
    var pwd = 'client';
    var authbasic = btoa(user + ':' + pwd);

    return axios.create({
        baseURL: servidor + '/pdrservice/oauth/',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/www-x-form-urlencoded',
            'Authorization': 'Basic ' + authbasic
        }
    });
}