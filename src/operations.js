import Api from '../src/api';
import apiTesting from '../src/apiTesting';
import apiOauth from '../src/apiOauth';
import apiBloqueo from '../src/apiBloqueo';
//import store from '../src/store';
import servidor from '../src/config';


//https://alfilatov.com/posts/run-chrome-without-cors/
//https://jgraph.github.io/mxgraph/javascript/examples/grapheditor/www/index.html

export default {
    parametros(params) {
        try {
            return Api(servidor()).get("parametros/getConfiguraciones?clave=getAll");
        } catch (error) {

            console.log(error);
        }
    },
    assignPar(params) {
        try {
            return Api(servidor()).post("parametros/saveConfiguraciones", {
                clave: params.clave,
                valor: params.valor,
                descripcion: params.descripcion,
                proceso: params.proceso,                
                usuario_actualiza: params.usuario
            });
        } catch (error) {
            console.log(error);
        }
    },
    getWord(params) {
        return Api(servidor()).get('/wordwise/entries?limit=1&headword=');
    },
    ejemplo(params) {
        try {
            return Api(servidor()).post("catalog/roles", {
                email: params.correo,
                password: params.contrasenia,
            });
        } catch (error) {
            console.log(error);
        }
    },
    roles(params) {
        try {
            return Api(servidor()).get("catalog/roles");
        } catch (error) {
            console.log(error.response);
        }
    },
    usersByRole(params) {
        try {
            return Api(servidor()).post("admin/usersbyrole", {
                role: '',
            });
        } catch (error) {

            console.log(error);
        }
    },
    assignrole(params) {
        try {
            return Api(servidor()).post("admin/assignrole", {
                role: params.role,
                username: params.username,
                nombreCompleto: params.nombre
            });
        } catch (error) {
            console.log(error);
        }
    },
    removerole(params) {
        try {
            return Api(servidor()).post("admin/removerole", {
                role: params.role,
                username: params.username
            });
        } catch (error) {
            console.log(error);
        }
    },
    processFile(params) {
        try {
            return Api(servidor()).post("rings/processFile", {
                "excelB64": params.excelB64,
                "extension": params.extension
            });
        } catch (error) {
            console.log("---------error save ring base 64--------");
            console.log(error);
        }
    },
    saveRing(params) {

        console.log("---------save ring--------");
        console.log(params);
        if (params.anillo.estatus == 'EDICIÓN') params.anillo.estatus = 1;
        else if (params.anillo.estatus == 'VALIDADO') params.anillo.estatus = 2;
        else if (params.anillo.estatus == 'EN EJECUCIÓN') params.anillo.estatus = 3;
        else if (params.anillo.estatus == 'DESHABILIDADO') params.anillo.estatus = 4;
        else if (params.anillo.estatus == 'ARCHIVADO') params.anillo.estatus = 5;

        if (params.anillo.estatus == '1') params.anillo.estatus = 1;
        else if (params.anillo.estatus == '2') params.anillo.estatus = 2;
        else if (params.anillo.estatus == '3') params.anillo.estatus = 3;
        else if (params.anillo.estatus == '4') params.anillo.estatus = 4;
        else if (params.anillo.estatus == '5') params.anillo.estatus = 5;




        try {
            return Api(servidor()).post("rings/save", {
                "id": params.anillo.id,
                "anillo": params.anillo.anillo,
                "estatus": params.anillo.estatus,
                "noNodos": params.anillo.noNodos,
                "version": params.anillo.version,
                "username": params.username,
                "nodos": params.anillo.nodos
            });
        } catch (error) {
            console.log("---------error save ring--------");
            console.log(error);
        }
    },
    getAnillosHeader(params) {
        try {
            // console.log(params.aux);
            return Api(servidor()).post("rings/", {
                "configStatus": '0',
            });
        } catch (error) {

            console.log(error);
        }
    },
    getAnillosDetalle(params) {

        try {
            return Api(servidor()).post("rings/details", {
                "configName": params.anillox,
                "configStatus": 0
            });
        } catch (error) {
            console.log(error);
        }
    },
    consultarRfc(params) {
        try {
            return apiTesting(servidor()).post("ticketAPI/consultarRFC", {
                "rfc": params.rfc,
            });
        } catch (error) {
            debugger;
            console.log(error);
        }
    },
    validacionacceso(params) {
        try {
            return apiTesting(servidor()).post("proxyStep/validacionacceso", {
                "anillos": params.validacionacceso.anillos,
                "usuario": params.validacionacceso.usuario,
                "fechahora": params.validacionacceso.fechahora,
                "servicios": params.validacionacceso.servicios,
                "updownlist": params.validacionacceso.updownlist,
                "rfc": params.validacionacceso.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    validacionpermisos(params) {
        try {
            console.log(params);
            return apiBloqueo(servidor()).post("validatePermissions", {
                "ip": params.parametros.ip,
                "equipo": params.parametros.equipo,
                "user": params.parametros.user,
                "password": params.parametros.password,
                "sshport": params.parametros.sshport,
                "interfaceName": params.parametros.interfaceName,
                "rfc": params.parametros.rfc,
                "idAnillo": params.parametros.idAnillo,
                "anillo": params.parametros.anillo
            });
        } catch (error) {
            console.log(error);
        }
    },
    cambiaEstatusAnillo(params) {
        //console.log("cambiaEstatusAnillo:::");
        //console.log(params);
        try {
            return Api(servidor()).post("rings/changestatus", {
                "id": params.idAnillo,
                "configStatus": params.estatus,
            });
        } catch (error) {
            console.log(error);
        }
    },
    //PAUL
    getIpRfc(rfc) {
        try {
            return Api(servidor()).post("rings/getIpRfc?rfc=" + rfc);
        } catch (error) {
            console.log(error)
        }

    },
    getIpAnillo(anillo) {
        // console.log("getIpAnillos:::");
        try {
            return Api(servidor()).post("rings/getIpAnillos?anillo=" + anillo);
        } catch (error) {
            console.log(error)
        }

    },
    //
    searchEvidencia(params) {
        try {
            return Api(servidor()).post("rings/getEvidences", {
                "anillo": params.anillo,
                "fechaEjecucion": params.fechaEjecucion,
                "tipoPrueba": params.tipoPrueba,
                "descripcion": params.descripcion,
                "rfc": params.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    generarToken(params) {
        try {
            return apiOauth(servidor()).post("token?grant_type=password&username=" + params.user + "&password=" + params.user + "&Basic", {
                "username": 'client',
                "password": 'client'
            });
        } catch (error) {
            console.log(error);
        }
    },
    bloqueaAnillo(params) {
        try {
            return apiBloqueo(servidor()).post("update/bloqueo?acces_token=" + params.token, {
                "anillos": [{ "anillo": params.idAnillo }],
                "bloqueo": params.bloqueo
            });
        } catch (error) {
            console.log(error);
        }
    },
    REQPROXY2(params) {
        try {
            if (params.operacion == "datosPing") {
                // console.log("================================");
                // console.log(params.parametros.reqProxy);
            }

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "anillo": params.parametros.anillo,
                "tipoProceso": params.parametros.tipoProceso,
                "fecha": params.parametros.fecha,
                "rfc": params.parametros.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    REQPROXY_VRRPD(params) {
        try {
            //console.log(params);
            return apiTesting(servidor()).post("/validates/vrrpCnx2dle", {
                "anillo": params.parametros.anillo,
                "idAnillo": params.parametros.idAnillo,
                "rfc": params.parametros.rfc,
                "user": params.parametros.user,
                "vrrps": [{
                        "sshport": params.parametros.vrrps[0].sshport,
                        "equipo": params.parametros.vrrps[0].equipo,
                        "ip": params.parametros.vrrps[0].ip,
                        "vrrp": params.parametros.vrrps[0].vrrp,
                        "password": params.parametros.vrrps[0].password,
                        "user": params.parametros.vrrps[0].user
                    },
                    {
                        "sshport": params.parametros.vrrps[1].sshport,
                        "equipo": params.parametros.vrrps[1].equipo,
                        "ip": params.parametros.vrrps[1].ip,
                        "vrrp": params.parametros.vrrps[1].vrrp,
                        "password": params.parametros.vrrps[1].password,
                        "user": params.parametros.vrrps[1].user
                    }
                ]
            });
        } catch (error) {
            console.log(error);
        }
    },
    REQPROXY_DATOS(params) {
        try {
            //console.log(params);
            return apiTesting(servidor()).post("proxyModels/" + params.operacion, {
                "ip": params.parametros.ip,
                "equipo": params.parametros.equipo,
                "user": params.parametros.user,
                "password": params.parametros.password,
                "sshport": params.parametros.sshport,
                "interfaceName": params.parametros.interfaceName,
                "gwsshport": params.parametros.gwsshport,
                "gwpass": params.parametros.gwpass,
                "gwip": params.parametros.gwip,
                "gwuser": params.parametros.gwuser,
                "rfc": params.parametros.rfc,
                "idAnillo": params.parametros.idAnillo,
                "anillo": params.parametros.anillo
            });
        } catch (error) {
            console.log(error);
        }
    },
    REQPROXY_VRRP_HOMOLOGADO(params) {
        try {
            //console.log(params);
            return apiTesting(servidor()).post("/proxyModels/validateVRRP", {
                "ip": params.parametros.ip,
                "equipo": params.parametros.equipo,
                "user": params.parametros.user,
                "password": params.parametros.password,
                "sshport": params.parametros.sshport,
                "vrrp": params.parametros.vrrp,
                "rfc": params.parametros.rfc,
                "idAnillo": params.parametros.idAnillo,
                "anillo": params.parametros.anillo,
            });
        } catch (error) {
            console.log(error);
        }
    },
    REQPROXY_OSPF(params) {
        try {
            //console.log(params);
            return apiTesting(servidor()).post("/proxyStep/datosOSPFv2", {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "anillo": params.parametros.anillo,
                "tipoProceso": params.parametros.tipoProceso,
                "rfc": params.parametros.rfc,
                "fecha": params.parametros.fecha,
            });
        } catch (error) {
            console.log(error);
        }

    },
    REQPROXY(params) {
        try {

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "anillo": params.parametros.anillo,
                "tipoProceso": params.parametros.tipoProceso,
                "rfc": params.parametros.rfc,
                "fecha": params.parametros.fecha,
            });


        } catch (error) {
            console.log(error);
        }
    },
    DOWN(params) {
        try {

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "fecha": params.parametros.fecha,
                "ipEquipo": params.parametros.ipEquipo,
                "interfaz": params.parametros.interfaz,
                "usuarioEquipo": params.parametros.usuarioEquipo,
                "pwdEquipo": params.parametros.pwdEquipo,
                "anillo": params.parametros.anillo,
                "equipo": params.parametros.equipo,
                "tipoProceso": params.parametros.tipoProceso,
                "interfaces": params.parametros.interfaces,
            });
        } catch (error) {
            console.log(error);
        }
    },
    UP2COMMIT(params) {
        try {

            return apiTesting(servidor()).post("proxyModels/executeDownUpWithCommit", {
                "ip": params.parametros.reqProxy.anillos.equipos[0].ip,
                "equipo": params.parametros.reqProxy.anillos.equipos[0].equipo,
                "user": params.parametros.reqProxy.anillos.equipos[0].user,
                "password": params.parametros.reqProxy.anillos.equipos[0].pwd,
                "sshport": 20444,
                "interfaceName": params.parametros.reqProxy.anillos.equipos[0].interfaces[0].interfaz,//.replaceAll('/', '\/'),
                "accion": "up",
                "rfc":params.parametros.rfc,
                "idAnillo": params.parametros.idAnillo,
                "anillo": params.parametros.anillo
            });
        } catch (error) {
            console.log(error);
        }
    },
    DOWN2COMMIT(params) {
        try {

            return apiTesting(servidor()).post("proxyModels/executeDownUpWithCommit", {
                "ip": params.parametros.reqProxy.anillos.equipos[0].ip,
                "equipo": params.parametros.reqProxy.anillos.equipos[0].equipo,
                "user": params.parametros.reqProxy.anillos.equipos[0].user,
                "password": params.parametros.reqProxy.anillos.equipos[0].pwd,
                "sshport": 20444,
                "interfaceName": params.parametros.reqProxy.anillos.equipos[0].interfaces[0].interfaz,//.replaceAll('/', '\/'),
                "accion": "down",
                "rfc":params.parametros.rfc,
                "idAnillo": params.parametros.idAnillo,
                "anillo": params.parametros.anillo
            });
        } catch (error) {
            console.log(error);
        }
    },
    DOWN2(params) {
        try {

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "fecha": params.parametros.fecha,
                "ipEquipo": params.parametros.ipEquipo,
                "interfaz": params.parametros.interfaz,
                "usuarioEquipo": params.parametros.usuarioEquipo,
                "pwdEquipo": params.parametros.pwdEquipo,
                "anillo": params.parametros.anillo,
                "equipo": params.parametros.equipo,
                "tipoProceso": params.parametros.tipoProceso,
                "interfaces": params.parametros.interfaces,
                "rfc": params.parametros.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    UP2(params) {
        try {

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "fecha": params.parametros.fecha,
                "ipEquipo": params.parametros.ipEquipo,
                "interfaz": params.parametros.interfaz,
                "usuarioEquipo": params.parametros.usuarioEquipo,
                "pwdEquipo": params.parametros.pwdEquipo,
                "anillo": params.parametros.anillo,
                "equipo": params.parametros.equipo,
                "tipoProceso": params.parametros.tipoProceso,
                "interfaces": params.parametros.interfaces,
                "rfc": params.parametros.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    UP(params) {
        try {

            return apiTesting(servidor()).post("proxyStep/" + params.operacion, {
                "reqProxy": params.parametros.reqProxy,
                "userlw": params.parametros.userlw,
                "pwdlw": params.parametros.pwdlw,
                "iplw": params.parametros.iplw,
                "fecha": params.parametros.fecha,
                "ipEquipo": params.parametros.ipEquipo,
                "interfaz": params.parametros.interfaz,
                "usuarioEquipo": params.parametros.usuarioEquipo,
                "pwdEquipo": params.parametros.pwdEquipo,
                "anillo": params.parametros.anillo,
                "equipo": params.parametros.equipo,
                "tipoProceso": params.parametros.tipoProceso,
                "interfaces": params.parametros.interfaces,
            });
        } catch (error) {
            console.log(error);
        }
    },
    Login(params) {
        try {
            return Api(servidor()).post("admin/doLogin", {
                "password": params.password,
                "userName": params.username
            });
        } catch (error) {
            console.log(error);
        }
    },
    userLog(params) {
        try {
            return Api(servidor()).post("admin/userLog", {
                "funcionality": params.funcionality,
                "username": params.username
            });
        } catch (error) {
            console.log(error);
        }
    },
    outputFileEvidence(params) {
        return Api(servidor()).get('rings/outputFileEvidence?evidenceId=' + params.evidenceId);
    },
    evidencesLog(params) {
        console.log(params);
        try {
            return Api(servidor()).post("rings/evidences", {
                "anillo": params.params.anillo,
                "anilloId": params.params.anilloId,
                "descripcion": "LOG DE CONFIGURACIÓN DE ANILLOS",
                "equipo": "",
                "errorLevel": "0",
                "fechaHora": "",
                "ip": "",
                "outFileB64": params.params.outFileB64,
                "tipoPrueba": "LOG",
                "username": params.params.username,
                "rfc": params.params.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    evidencesAnilloLog(params) {
        try {
            return Api(servidor()).post("rings/evidences", {
                "anillo": params.params.anillo,
                "anilloId": params.params.anilloId,
                "descripcion": "CONFIGURACIÓN DEL ANILLO",
                "equipo": "",
                "errorLevel": "0",
                "fechaHora": "",
                "ip": "",
                "outFileB64": params.params.outFileB64,
                "tipoPrueba": "CONFIG",
                "username": params.params.username
            });
        } catch (error) {
            console.log(error);
        }
    },
    evidencesAnilloLog2(params) {
        try {
            return Api(servidor()).post("rings/evidences", {
                "anillo": params.params.anillo,
                "anilloId": params.params.anilloId,
                "descripcion": "CONFIGURACIÓN DEL ANILLO",
                "equipo": "",
                "errorLevel": "0",
                "fechaHora": "",
                "ip": "",
                "outFileB64": params.params.outFileB64,
                "tipoPrueba": "CONFIG",
                "username": params.params.username,
                "rfc": params.params.rfc
            });
        } catch (error) {
            console.log(error);
        }
    },
    evidencesRFCLog(params) {
        try {
            return Api(servidor()).post("rings/evidences", {
                "anillo": params.params.anillo,
                "anilloId": params.params.anilloId,
                "descripcion": "RFC DE EJECUCIÓN DEL ANILLO",
                "equipo": "",
                "errorLevel": "0",
                "fechaHora": "",
                "ip": "",
                "outFileB64": params.params.outFileB64,
                "tipoPrueba": "RFC",
                "username": params.params.username
            });
        } catch (error) {
            console.log(error);
        }
    },
    evidencesRFCLogInvalido(params) {
        try {
            return Api(servidor()).post("rings/evidences", {
                "anillo": params.params.anillo,
                "anilloId": params.params.anilloId,
                "descripcion": "RFC INVÁLIDO PARA LA EJECUCIÓN DEL ANILLO",
                "equipo": "",
                "errorLevel": "0",
                "fechaHora": "",
                "ip": "",
                "outFileB64": params.params.outFileB64,
                "tipoPrueba": "RFC",
                "username": params.params.username,
                "rfc": params.params.rfc,
            });
        } catch (error) {
            console.log(error);
        }
    },
    LDAP(params) {

        try {
            return apiTesting(servidor()).post("ldap", {
                "user": params.user,
                "password": params.password
            });
        } catch (error) {
            console.log(error);
        }
    },
    getFuncionalidades(params) {
        return Api(servidor()).get('admin/functionalities?username=' + params.username);
    },
    getanchoBanda(params) {

        return apiTesting(servidor()).get("performance?period=1h&hostname=" + params.hostname + "&ip=" + params.ip + "&username=" + params.username);
    },
    getCredentials(params) {
        try {
            return Api(servidor()).post("rings/getCredentials?anilloId=" + params.anilloId, {
                "anilloId": ''
            });
        } catch (error) {
            console.log(error);
        }
    },
    credentials(params) {
        //console.log("credenciales post");
        //  console.log(params.objCredenctials.userEquipo);
        try {
            return Api(servidor()).post("rings/credentials", {
                "anilloId": params.objCredenctials.anilloId,
                "userEquipo": params.objCredenctials.userEquipo,
                "pwdEquipo": params.objCredenctials.pwdEquipo,
                "userLanWan": params.objCredenctials.userLanWan,
                "pwdLanwan": params.objCredenctials.pwdLanwan,
                "userColector": params.objCredenctials.userColector,
                "pwdColector": params.objCredenctials.pwdColector,
                "userOlt": params.objCredenctials.userOlt,
                "pwdOlt": params.objCredenctials.pwdOlt,
                "userUrlVideo": params.objCredenctials.userUrlVideo,
                "pwdUrlVideo": params.objCredenctials.pwdUrlVideo
            });
        } catch (error) {
            console.log(error);
        }
    }
}