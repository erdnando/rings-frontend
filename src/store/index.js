import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        ospfs:[],
        omitidoPorIPColector:true,
        ultimaRefAlRouter:'',
        ultimaRefAlRouterValor:'',
        stepFalloColocarEnVerde: false,
        arrVueltas: [],
        arrSteps: [],
        TotalVueltas: 0,
        PorcentajePorVuelta: 0,
        PorcentajePorStep: 0,
        StepActual: 0,
        VueltaActual: 0,
        contadorDesfaseValInicial: 0,
        contadorDesfaseEjecucion: 0,
        incrementoBarraValInicial: (100 / 11) / 50,
        totalStepsEjecucion: 0,
        incrementoBarraEjecucion: 0,
        errorForzado: true,
        retomadas: 0,
        setStepGreen: true,
        trabajandoConNodo: null,
        trabajandoConNodoX: null,
        segmentoBuscado: '',
        templateAnillo: {},
        arrEquiposRef: [],
        timerPerformance: '',
        pingLanWanTemplate: '',
        omiteValInicial: true,

        formValInicialx: false,
        formValPingsx: false,
        valInicialAvancePing: [0, 0, 0, 0, 0],
        address: [],
        ipsAnillos: [],

        autorizacion: false,
        arrequiposOriginal: [],
        inicialKey: 0,
        indexEdit: 0,
        anilloSeleccionado: null,
        anilloSeleccionadoE: null,
        anilloEstatus: '',
        sesion: {
            token: '',
            autenticado: false,
            usuario: '',
            pwd: '',
            role: '',
            menu: [],
            historialEvidencias: false,
            userAdmin: false,
            ejecucionAnillo: false,
            configAnillos: false,
            parametros: false,
        },
        templateXML: [
            //https://jgraph.github.io/mxgraph/javascript/examples/grapheditor/www/index.html
            {
                nodoAnillo: 1,
                xml: '<mxGraphModel dx="1426" dy="740" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169" math="0" shadow="0">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="260" y="200" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="240" y="100" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="260" y="180" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-13;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="272" y="120" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="340" y="110" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 2,
                xml: '<mxGraphModel dx="1224" dy="704" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169" math="0" shadow="0">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="344" y="75" as="sourcePoint" />' +
                    '<mxPoint x="264" y="175" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="340" y="235" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=-100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="245" y="140" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="322" y="140" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="172.99999999999997" y="30" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="342" y="220" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="174.99999999999997" y="130" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-13;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="354" y="160" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="193.99999999999997" y="59" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="415" y="160" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="134" y="59" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 3,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="302.46" y="430" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="133.46" y="342" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=-100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="352.46" y="150" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="282.46" y="342" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="133.45999999999998" y="232" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="272.46" y="50" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="302.46" y="422" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="135.45999999999998" y="332" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="282.46" y="138.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-13;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="314.46" y="362" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-12;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="304.46" y="74" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="370" y="362" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="99" y="261" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="248" y="70" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=12;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.75;exitDx=0;exitDy=0;" parent="1" source="77" target="79" edge="1" class="txtlineaConec txtlineaConec_n3-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="-7.540000000000006" y="132" as="sourcePoint" />' +
                    '<mxPoint x="44.459999999999994" y="47" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="471.46" y="180" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=0;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="84.46" y="237" as="sourcePoint" />' +
                    '<mxPoint x="134.45999999999998" y="137" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="300" y="120" />' +
                    '<mxPoint x="201.45999999999998" y="170" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="304.46" y="277" as="sourcePoint" />' +
                    '<mxPoint x="224.45999999999998" y="377" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="154.45999999999998" y="261" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 4,
                xml: '<mxGraphModel dx="1210" dy="691" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="2" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=0.14;entryY=0.998;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;entryPerimeter=0;" parent="1" source="23" target="24" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="65" y="319" as="sourcePoint" />' +
                    '<mxPoint x="115" y="219" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="163" y="184" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="3" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="2" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="6" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=1;entryY=0.25;entryDx=0;entryDy=0;exitX=0;exitY=1;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="25" target="22" edge="1" class="txtlineaConec txtlineaConec_n4-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="95" y="109" as="sourcePoint" />' +
                    '<mxPoint x="513" y="266" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="463" y="364" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="7" value="N4-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontColor=#1A1A1A;" parent="6" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0907" y="-1" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="8" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="22" target="23" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="285" y="359" as="sourcePoint" />' +
                    '<mxPoint x="205" y="459" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="203" y="364" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="9" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="8" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="10" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-2;spacingRight=0;spacingLeft=97;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="287.39" y="517" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="11" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-2;spacingRight=0;spacingLeft=97;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="100" y="340" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="12" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-12;spacingRight=0;spacingLeft=97;fontStyle=1;fontSize=15;spacingBottom=-10;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="260.39" y="150" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="13" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-12;spacingRight=0;spacingLeft=97;fontStyle=1;fontSize=15;spacingBottom=-10;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="480" y="328.64" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="14" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="272" y="423" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="15" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="84" y="227.64" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="16" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="232.39" y="54.000000000000014" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="17" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="461" y="232.74" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="18" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="283" y="504" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="19" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="92" y="328.64" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="20" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="248.39" y="140" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="468" y="322.42" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="295" y="444" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="111" y="257.64" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=14;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="260.39" y="78.00000000000003" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="483" y="261" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=-7;spacingRight=0;spacingLeft=1;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="374" y="444" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=6;spacingRight=0;spacingLeft=1;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="54" y="267.64" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=8;spacingRight=0;spacingLeft=1;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="200" y="58" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=1;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="550" y="250" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="4" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=1;entryY=0.25;entryDx=0;entryDy=0;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="24" target="25" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="274" as="sourcePoint" />' +
                    '<mxPoint x="25" y="129" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="480" y="140" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="5" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontColor=#1A1A1A;" parent="4" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1074" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 5,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="163" y="327" as="sourcePoint" />' +
                    '<mxPoint x="213" y="227" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="77" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="71" y="222" as="sourcePoint" />' +
                    '<mxPoint x="123" y="137" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0.5;entryY=0;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="193" y="117" as="sourcePoint" />' +
                    '<mxPoint x="243" y="17" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="570" y="120" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=0;exitY=1;exitDx=0;exitDy=0;" parent="1" source="70" target="79" edge="1" class="txtlineaConec txtlineaConec_n5-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="349" y="81" as="sourcePoint" />' +
                    '<mxPoint x="399" y="-19" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="591" y="342" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="383" y="367" as="sourcePoint" />' +
                    '<mxPoint x="303" y="467" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="381" y="522" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=-100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="280" y="442" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=-100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="214" y="268.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="126" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=-100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="470" y="150" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="127" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="563" y="295" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="361" y="432" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="212" y="322" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="141" y="171.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="395" y="53" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="550" y="205.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="381" y="512" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="214" y="422" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="151" y="260" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="405" y="139" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="563" y="285.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-13;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="393" y="452" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="233" y="351" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-12;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="173" y="195.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="420" y="75" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="581" y="226" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="446" y="452" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="180" y="351" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="120" y="191.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="364" y="60" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="631" y="225.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 6,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.25;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="191" y="98" as="sourcePoint" />' +
                    '<mxPoint x="241" y="-2" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="32.870000000000005" y="18.77" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="381" y="348" as="sourcePoint" />' +
                    '<mxPoint x="301" y="448" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="357" y="502" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="201" y="413" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="149" y="251" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="126" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="361" y="130" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="127" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="528" y="190.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="347" y="413" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="210" y="303" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="139" y="152.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="357" y="34" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="531" y="98" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="542" y="260" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="362" y="493" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="212" y="403" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="149" y="241" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="361" y="120" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="533" y="181" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="544" y="350" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-13;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="374" y="433" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="231" y="332" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-12;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="171" y="176.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="382" y="56" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="551" y="118.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="567" y="285" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="430" y="430" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="171" y="332" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="115" y="167.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="320" y="46" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="614" y="104" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="630" y="282" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="113" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="544" y="364" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=0;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" parent="1" source="79" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="424" y="458" as="sourcePoint" />' +
                    '<mxPoint x="596.5" y="335" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="479" y="383" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="178" value="N6-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="177" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="161" y="308" as="sourcePoint" />' +
                    '<mxPoint x="211" y="208" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="77" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="69" y="203" as="sourcePoint" />' +
                    '<mxPoint x="121" y="118" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="347" y="62" as="sourcePoint" />' +
                    '<mxPoint x="397" y="-38" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="649" y="173" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 7,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="200" y="338" as="sourcePoint" />' +
                    '<mxPoint x="250" y="238" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="138" y="293" as="sourcePoint" />' +
                    '<mxPoint x="160" y="148" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="230" y="128" as="sourcePoint" />' +
                    '<mxPoint x="280" y="28" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="386" y="92" as="sourcePoint" />' +
                    '<mxPoint x="436" y="-8" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="474" y="77" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=1;exitY=0.25;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=0.5;entryY=1;entryDx=0;entryDy=0;" parent="1" source="79" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="641" y="365" as="sourcePoint" />' +
                    '<mxPoint x="588" y="193" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="638" y="383" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="-3.25" y="-15.85" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=0.75;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="710" y="118" as="sourcePoint" />' +
                    '<mxPoint x="627.5" y="23.003756729740587" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="410" y="533" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="255" y="444.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="98" y="354.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="126" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="306" y="237" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="127" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="458" y="133" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="113" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="602" y="230" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="117" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="572" y="438" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="398" y="443" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="249" y="333" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="78" y="260" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="296" y="138" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="451" y="39" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="595" y="127" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="567" y="331.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="418" y="523" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="251" y="433" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="98" y="343" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="306" y="224" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="453" y="122" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="597" y="217" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="567" y="424.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="430" y="463" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=14;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="110" y="284" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="321" y="160" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="471" y="59.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="620" y="152" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-3;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="589" y="354.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="185" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="370" y="460.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="183" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="215" y="370.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="182" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="54" y="280" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="181" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="260" y="152" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="180" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="537" y="53" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="184" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="677" y="150" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="186" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="645" y="360" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="420" y="378" as="sourcePoint" />' +
                    '<mxPoint x="340" y="478" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="270" y="362" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 8,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="182" y="327" as="sourcePoint" />' +
                    '<mxPoint x="232" y="227" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="120" y="282" as="sourcePoint" />' +
                    '<mxPoint x="142" y="137" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="212" y="117" as="sourcePoint" />' +
                    '<mxPoint x="262" y="17" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="70" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="480" y="20" as="sourcePoint" />' +
                    '<mxPoint x="632.56" y="117.71375672974048" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="540" y="90" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="402" y="367" as="sourcePoint" />' +
                    '<mxPoint x="322" y="467" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=0.75;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="692" y="107" as="sourcePoint" />' +
                    '<mxPoint x="609.5" y="12.003756729740587" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="395" y="523" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="234" y="435" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="79" y="346" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="126" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="290" y="225" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="127" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="427" y="129" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="113" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-1;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="585" y="182" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="117" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="711" y="302" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="118" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=75;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="557" y="404" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="380" y="435" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="238" y="322" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="68" y="251" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="282" y="129" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="427" y="34" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="582" y="89" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="708" y="198" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="555" y="310" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="396" y="512" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="233" y="422" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="80" y="334" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="288" y="213" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="426" y="118" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="588" y="171" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="712" y="291" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="558" y="393" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="412" y="452" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="252" y="351" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=14;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="92" y="273" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="303" y="149" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="447" y="55.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="607" y="110" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-3;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="733" y="224" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="578" y="329" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="480" y="448" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="192" y="356" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="30" y="266" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="241" y="131" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="384" y="43" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="675" y="99" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="789" y="217" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="640" y="328" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=1;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="79" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="615" y="350.5662432702595" as="sourcePoint" />' +
                    '<mxPoint x="862" y="247.0037567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=1;exitY=0.5;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=0;entryY=1;entryDx=0;entryDy=0;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="852" y="161.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="570" y="182" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="680" y="312" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="-3.25" y="-15.85" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 9,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="182" y="383" as="sourcePoint" />' +
                    '<mxPoint x="232" y="283" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="120" y="338" as="sourcePoint" />' +
                    '<mxPoint x="142" y="193" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="26.99" y="-9.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="212" y="173" as="sourcePoint" />' +
                    '<mxPoint x="262" y="73" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="382" y="123" as="sourcePoint" />' +
                    '<mxPoint x="432" y="23" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="470" y="108" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="402" y="423" as="sourcePoint" />' +
                    '<mxPoint x="322" y="523" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=1;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="75" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n9">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="772" y="368.1362432702593" as="sourcePoint" />' +
                    '<mxPoint x="862" y="303.0037567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N9" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=0.25;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="852" y="217.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="742" y="146.22000000000003" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="730" y="238" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="-3.25" y="-15.85" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7" source="72">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="692" y="163" as="sourcePoint" />' +
                    '<mxPoint x="609.5" y="68.00375672974059" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="123" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="392" y="550" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="124" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="238" y="488" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="125" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="80" y="397" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="126" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="212" y="314" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="127" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="362" y="210" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="113" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="496" y="110" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="117" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="651" y="207" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="118" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="702" y="366" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="121" value="DOWN9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=0;spacingRight=0;spacingLeft=100;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN9" vertex="1">' +
                    '<mxGeometry x="546" y="468" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="367" y="458" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="238" y="378" width="100" height="40" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="60" y="310" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="212" y="225" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="352" y="118" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="496" y="16" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="670" y="110" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="704" y="270" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="111" value="interfaz9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF9_N9" vertex="1">' +
                    '<mxGeometry x="535" y="377" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="387" y="538" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="233" y="478" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="80" y="388" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="227" y="307" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="354" y="198" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="496" y="98" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="662" y="198" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="701" y="355" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="179" value="Ip9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP9" vertex="1">' +
                    '<mxGeometry x="544" y="457" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="399" y="478" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=3;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="252" y="407" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=14;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="92" y="329" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="242" y="243" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="372" y="135.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-17;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="517" y="33" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-3;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="687" y="128" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="721" y="291" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="75" value="nodo 9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-13;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo9" vertex="1">' +
                    '<mxGeometry x="565" y="396" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="453" y="478" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="202" y="407" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="42" y="322" width="50" height="53" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="178" y="222" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="312" y="130" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="462" y="20" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="742" y="128" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="775" y="290" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="BANDA9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA9" vertex="1">' +
                    '<mxGeometry x="620" y="396" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="88" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="75" edge="1" class="txtlineaConec txtlineaConec_n9-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="602" y="441.5662432702593" as="sourcePoint" />' +
                    '<mxPoint x="752" y="405.4337567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="101" value="N9-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="88" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4187" relative="1" as="geometry">' +
                    '<mxPoint x="17.09" y="-9.57" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 10,
                xml: '<mxGraphModel dx="1902" dy="1081" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="83" y="300" as="sourcePoint" />' +
                    '<mxPoint x="105" y="155" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="37.989999999999995" y="0.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="175" y="135" as="sourcePoint" />' +
                    '<mxPoint x="225" y="35" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="70" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="345" y="85" as="sourcePoint" />' +
                    '<mxPoint x="460" y="90" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="463" y="90" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="365" y="385" as="sourcePoint" />' +
                    '<mxPoint x="285" y="485" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=0.25;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="815" y="179.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="705" y="108.22000000000003" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="-3.25" y="-15.85" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="604" y="125" as="sourcePoint" />' +
                    '<mxPoint x="521.5" y="30.003756729740587" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="133" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="74" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="501" y="289" as="sourcePoint" />' +
                    '<mxPoint x="586" y="438" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="343" y="428" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="182.5" y="379" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="37" y="280" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="175" y="178" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="309" y="92" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="429.5" y="45" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="580" y="99" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="723" y="173" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="111" value="interfaz9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF9_N9" vertex="1">' +
                    '<mxGeometry x="660" y="334" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="112" value="interfaz10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF10_N10" vertex="1">' +
                    '<mxGeometry x="486" y="348" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="342" y="514" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="186" y="465" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="46" y="367" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="190" y="262" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="311" y="178" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="433.5" y="132" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="580" y="187" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="716.5" y="260" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="179" value="Ip9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP9" vertex="1">' +
                    '<mxGeometry x="654" y="422" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="180" value="Ip10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP10" vertex="1">' +
                    '<mxGeometry x="492" y="432" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="354" y="448" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-9;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="205" y="400" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="205" y="198" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="329" y="112.5" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="456" y="67" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-10;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="602" y="117" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="739" y="193" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="75" value="nodo 9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo9" vertex="1">' +
                    '<mxGeometry x="685" y="356" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="74" value="nodo 10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo10" vertex="1">' +
                    '<mxGeometry x="511" y="367" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="183" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="373" y="543" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="182" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-43;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="217.5" y="497" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="181" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="77" y="403" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="185" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-48;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="221" y="294" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="186" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="343" y="216" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="187" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-47;spacingRight=0;spacingLeft=2;fontStyle=1;fontSize=15;spacingBottom=-6;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="471" y="161" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="189" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=4;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="614" y="220" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="190" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="753" y="291" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="188" value="DOWN9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN9" vertex="1">' +
                    '<mxGeometry x="704" y="451" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="184" value="DOWN10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN10" vertex="1">' +
                    '<mxGeometry x="528" y="460.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="413" y="450.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="150" y="402" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="12" y="302" width="50" height="53" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="144" y="188" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="271" y="103" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="517" y="51" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="665" y="109.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="796" y="203" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="BANDA9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA9" vertex="1">' +
                    '<mxGeometry x="743" y="358.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="20" value="BANDA10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA10" vertex="1">' +
                    '<mxGeometry x="567" y="371.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=1;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="75" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n9">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="735" y="330.1362432702593" as="sourcePoint" />' +
                    '<mxPoint x="825" y="265.0037567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N9" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="88" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="74" target="75" edge="1" class="txtlineaConec txtlineaConec_n9-n10">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="625" y="430.5662432702593" as="sourcePoint" />' +
                    '<mxPoint x="715" y="367.4337567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="101" value="N9-N10" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="88" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4187" relative="1" as="geometry">' +
                    '<mxPoint x="17.09" y="-9.57" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="87" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="79" target="74" edge="1" class="txtlineaConec txtlineaConec_n10-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="505" y="567" as="sourcePoint" />' +
                    '<mxPoint x="617.5" y="511" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="102" value="N10-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="87" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="19.06" y="-21.16" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="145" y="345" as="sourcePoint" />' +
                    '<mxPoint x="195" y="245" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-19;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="66" y="301" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 11,
                xml: '<mxGraphModel dx="1146" dy="1037" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="20" value="BANDA10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA10" vertex="1">' +
                    '<mxGeometry x="741" y="277.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="196" value="BANDA11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA11" vertex="1">' +
                    '<mxGeometry x="580" y="376.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="445" y="451.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="117" y="208" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=0.5;exitY=1;exitDx=0;exitDy=0;" parent="1" source="105" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="83" y="300" as="sourcePoint" />' +
                    '<mxPoint x="105" y="155" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="-5.910000000000004" y="5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="175" y="135" as="sourcePoint" />' +
                    '<mxPoint x="225" y="35" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="373" y="140.12286759728545" as="sourcePoint" />' +
                    '<mxPoint x="291" y="81" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="365" y="385" as="sourcePoint" />' +
                    '<mxPoint x="285" y="485" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=0;exitY=0.5;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="815" y="179.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="705" y="108.22000000000003" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="28.68" y="23.199999999999996" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=0.836;entryY=0.491;entryDx=0;entryDy=0;entryPerimeter=0;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="221" y="83.62286759728522" as="sourcePoint" />' +
                    '<mxPoint x="137" y="59" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="133" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="74" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="673" y="197" as="sourcePoint" />' +
                    '<mxPoint x="758" y="346" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="364" y="426.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="200.5" y="374" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="37" y="301" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="148" y="198" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="263" y="113" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="386.5" y="52" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="541.5" y="23" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="111" value="interfaz9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF9_N9" vertex="1">' +
                    '<mxGeometry x="835" y="158" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="112" value="interfaz10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF10_N10" vertex="1">' +
                    '<mxGeometry x="659" y="259" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="364" y="514" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="204" y="460" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="46" y="388" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="163" y="282" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="279" y="203" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="384" y="138" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="529" y="113" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="670" y="159" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="179" value="Ip9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP9" vertex="1">' +
                    '<mxGeometry x="836" y="244" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="180" value="Ip10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP10" vertex="1">' +
                    '<mxGeometry x="664" y="340" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-19;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="66" y="322" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="178" y="218" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="297" y="137.5" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-10;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="551" y="43" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="75" value="nodo 9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo9" vertex="1">' +
                    '<mxGeometry x="852" y="178" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="74" value="nodo 10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo10" vertex="1">' +
                    '<mxGeometry x="683" y="275" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="183" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="403" y="543" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="182" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-43;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="235.5" y="492" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="181" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="77" y="423" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="185" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-48;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="194" y="315" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="186" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="311" y="239" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="187" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-47;spacingRight=0;spacingLeft=2;fontStyle=1;fontSize=15;spacingBottom=-6;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="419" y="167" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="189" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=4;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="563" y="146" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="190" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="707" y="190" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="188" value="DOWN9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN9" vertex="1">' +
                    '<mxGeometry x="871" y="273" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="184" value="DOWN10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN10" vertex="1">' +
                    '<mxGeometry x="700" y="368.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="168" y="397" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="12" y="323" width="50" height="53" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="239" y="128" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="341" y="51" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="496.5" y="13" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="BANDA9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA9" vertex="1">' +
                    '<mxGeometry x="910" y="180.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=1;entryY=1;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;" parent="1" source="75" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n9">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="735" y="330.1362432702593" as="sourcePoint" />' +
                    '<mxPoint x="825" y="265.0037567297407" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N9" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="32.61" y="21.85" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="88" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="74" target="75" edge="1" class="txtlineaConec txtlineaConec_n9-n10">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="625" y="430.5662432702593" as="sourcePoint" />' +
                    '<mxPoint x="715" y="367.4337567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="101" value="N9-N10" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="88" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4187" relative="1" as="geometry">' +
                    '<mxPoint x="17.09" y="-9.57" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="191" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="194" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="509" y="292" as="sourcePoint" />' +
                    '<mxPoint x="594" y="441" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="192" value="interfaz11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF11_N11" vertex="1">' +
                    '<mxGeometry x="494" y="351" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="193" value="Ip11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP11" vertex="1">' +
                    '<mxGeometry x="500" y="435" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="195" value="DOWN11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN11" vertex="1">' +
                    '<mxGeometry x="536" y="463.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="197" value="N10-N11" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="1" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="609.4297459621555" y="400.55286759728517" as="geometry">' +
                    '<mxPoint x="2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="693" y="92" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="750" y="96" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="401" y="73" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="672.0000000000001" y="72" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="87" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="194" target="74" edge="1" class="txtlineaConec txtlineaConec_n10-n11">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="392" y="322" as="sourcePoint" />' +
                    '<mxPoint x="579" y="357.3771324027148" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="102" value="N10-N11" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="87" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="16.73" y="-24.72" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="194" value="nodo 11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo11" vertex="1">' +
                    '<mxGeometry x="519" y="370" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="198" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="79" target="194" edge="1" class="txtlineaConec txtlineaConec_n11-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="422" y="419.2428675972852" as="sourcePoint" />' +
                    '<mxPoint x="490" y="380" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="199" value="N11-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="198" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="17.849999999999998" y="-17.22" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="384" y="448" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="145" y="345" as="sourcePoint" />' +
                    '<mxPoint x="195" y="245" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-9;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="223" y="395" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 12,
                xml: '<mxGraphModel dx="1146" dy="1037" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="285" y="198" as="sourcePoint" />' +
                    '<mxPoint x="271" y="21" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.75" y="-14.73" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="424" y="149.12286759728522" as="sourcePoint" />' +
                    '<mxPoint x="342" y="90" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;entryX=1;entryY=0.25;entryDx=0;entryDy=0;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="388.76" y="398.3128675972852" as="sourcePoint" />' +
                    '<mxPoint x="300" y="310" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="270" y="440" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="221" y="83.62286759728522" as="sourcePoint" />' +
                    '<mxPoint x="137" y="59" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="560" y="70" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="133" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="74" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="756" y="176" as="sourcePoint" />' +
                    '<mxPoint x="841" y="325" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="295" y="430" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="170.5" y="381" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="37" y="277" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="178" y="186" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="316" y="106" width="100" height="17" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="448" y="48" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="578" y="19.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="761.0000000000001" y="41" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="111" value="interfaz9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF9_N9" vertex="1">' +
                    '<mxGeometry x="896" y="142" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="112" value="interfaz10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF10_N10" vertex="1">' +
                    '<mxGeometry x="741" y="235" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="309" y="516" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="157" y="466" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="46" y="367" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="187" y="270" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="321" y="189" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="450.5" y="130" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="590" y="106" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="739.5" y="131" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="179" value="Ip9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP9" vertex="1">' +
                    '<mxGeometry x="903" y="228" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="180" value="Ip10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP10" vertex="1">' +
                    '<mxGeometry x="747" y="319" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="321" y="450" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-9;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="186" y="400" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-19;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="66" y="301" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="208" y="205" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="343" y="123.5" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-10;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="608" y="36" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="74" value="nodo 10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo10" vertex="1">' +
                    '<mxGeometry x="766" y="254" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="183" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="340" y="547" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="182" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-43;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="198.5" y="498" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="181" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="77" y="403" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="185" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-48;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="224" y="305" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="186" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="357" y="227" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="187" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-47;spacingRight=0;spacingLeft=2;fontStyle=1;fontSize=15;spacingBottom=-6;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="488" y="159" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="189" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=4;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="620" y="139" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="190" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="778" y="166" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="188" value="DOWN9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN9" vertex="1">' +
                    '<mxGeometry x="936" y="259" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="184" value="DOWN10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN10" vertex="1">' +
                    '<mxGeometry x="783" y="347.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="131" y="402" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="12" y="302" width="50" height="53" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="147" y="195" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="285" y="114" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="416" y="57" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="664" y="21.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="BANDA9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA9" vertex="1">' +
                    '<mxGeometry x="975" y="166.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="191" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="194" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="611" y="274" as="sourcePoint" />' +
                    '<mxPoint x="696" y="423" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="192" value="interfaz11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF11_N11" vertex="1">' +
                    '<mxGeometry x="582" y="329" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="193" value="Ip11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP11" vertex="1">' +
                    '<mxGeometry x="602" y="417" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="194" value="nodo 11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo11" vertex="1">' +
                    '<mxGeometry x="621" y="352" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="195" value="DOWN11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN11" vertex="1">' +
                    '<mxGeometry x="638" y="445.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="196" value="BANDA11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA11" vertex="1">' +
                    '<mxGeometry x="682" y="358.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="826" y="62" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="20" value="BANDA10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA10" vertex="1">' +
                    '<mxGeometry x="824" y="256.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="380" y="452.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="473" y="65" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="202" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=0.5;exitY=0;exitDx=0;exitDy=0;" parent="1" source="207" target="194" edge="1" class="txtlineaConec txtlineaConec_n11-n12">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="402" y="485" as="sourcePoint" />' +
                    '<mxPoint x="613" y="474.3771324027148" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="203" value="N11-N12" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="202" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="-20.389999999999993" y="-50.22" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="204" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="207" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="481" y="375" as="sourcePoint" />' +
                    '<mxPoint x="566" y="524" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="205" value="interfaz12" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF12_N12" vertex="1">' +
                    '<mxGeometry x="465" y="434" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="206" value="Ip12" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP12" vertex="1">' +
                    '<mxGeometry x="472" y="518" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="208" value="DOWN12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN12" vertex="1">' +
                    '<mxGeometry x="508" y="546.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="209" value="BANDA12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA12" vertex="1">' +
                    '<mxGeometry x="552" y="459.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="198" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="79" target="207" edge="1" class="txtlineaConec txtlineaConec_n12-n1">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="422" y="419.2428675972852" as="sourcePoint" />' +
                    '<mxPoint x="490" y="540" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="430" y="440" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="199" value="N12-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="198" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="29.73" y="17.28" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="207" value="nodo 12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo12" vertex="1">' +
                    '<mxGeometry x="491" y="453" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="87" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="194" target="74" edge="1" class="txtlineaConec txtlineaConec_n10-n11">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="369" y="322" as="sourcePoint" />' +
                    '<mxPoint x="580" y="311.3771324027148" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="102" value="N10-N11" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="87" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="16.73" y="-24.72" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="88" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="74" target="75" edge="1" class="txtlineaConec txtlineaConec_n9-n10">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="625" y="430.5662432702593" as="sourcePoint" />' +
                    '<mxPoint x="715" y="367.4337567297407" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="101" value="N9-N10" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="88" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4187" relative="1" as="geometry">' +
                    '<mxPoint x="19.56" y="-13.620000000000001" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="83" y="300" as="sourcePoint" />' +
                    '<mxPoint x="105" y="155" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="37.989999999999995" y="0.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="145" y="345" as="sourcePoint" />' +
                    '<mxPoint x="195" y="245" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n9">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="930" y="190" as="sourcePoint" />' +
                    '<mxPoint x="825" y="265.0037567297407" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="860" y="130" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N9" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="75" value="nodo 9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo9" vertex="1">' +
                    '<mxGeometry x="920" y="164" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="766" y="64" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;exitX=0.25;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="815" y="179.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="705" y="108.22000000000003" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="38.47" y="22.769999999999996" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            },
            {
                noAnillo: 13,
                xml: '<mxGraphModel dx="1146" dy="1037" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="827" pageHeight="1169">' +
                    '<root>' +
                    '<mxCell id="0" />' +
                    '<mxCell id="1" parent="0" />' +
                    '<mxCell id="81" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=11;entryX=1;entryY=0.5;entryDx=0;entryDy=0;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0.25;exitY=0;exitDx=0;exitDy=0;" parent="1" source="78" target="77" edge="1" class="txtlineaConec txtlineaConec_n2-n3">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="145" y="345" as="sourcePoint" />' +
                    '<mxPoint x="195" y="245" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="94" value="N2-N3" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="81" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.0771" relative="1" as="geometry">' +
                    '<mxPoint y="-1" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="83" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0;exitDx=0;exitDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="76" target="70" edge="1" class="txtlineaConec txtlineaConec_n4-n5">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="175" y="135" as="sourcePoint" />' +
                    '<mxPoint x="225" y="35" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="96" value="N4-N5" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="83" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.5576" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="14.28" y="-4.23" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="85" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.25;exitDx=0;exitDy=0;" parent="1" source="70" target="71" edge="1" class="txtlineaConec txtlineaConec_n5-n6">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="370" y="149.12286759728522" as="sourcePoint" />' +
                    '<mxPoint x="288" y="90" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="97" value="N5-N6" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="85" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.3932" y="2" relative="1" as="geometry">' +
                    '<mxPoint x="12.42" y="-1.83" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="86" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="79" target="78" edge="1" class="txtlineaConec txtlineaConec_n1-n2">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="365" y="385" as="sourcePoint" />' +
                    '<mxPoint x="285" y="485" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="93" value="N1-N2" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="86" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.1784" y="2" relative="1" as="geometry">' +
                    '<mxPoint as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="90" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" source="73" target="72" edge="1" class="txtlineaConec txtlineaConec_n7-n8">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="815" y="179.78624327025932" as="sourcePoint" />' +
                    '<mxPoint x="705" y="108.22000000000003" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="99" value="N7-N8" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="90" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2695" y="-31" relative="1" as="geometry">' +
                    '<mxPoint x="18.98" y="44.059999999999995" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="91" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;elbow=vertical;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="72" target="71" edge="1" class="txtlineaConec txtlineaConec_n6-n7">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="221" y="83.62286759728522" as="sourcePoint" />' +
                    '<mxPoint x="137" y="59" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="490" y="60" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="98" value="N6-N7" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="91" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.0497" y="11" relative="1" as="geometry">' +
                    '<mxPoint x="3" y="-2" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="133" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="74" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="899" y="207" as="sourcePoint" />' +
                    '<mxPoint x="984" y="356" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="103" value="interfaz1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF1_N1" vertex="1">' +
                    '<mxGeometry x="364" y="441" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="104" value="interfaz2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF2_N2" vertex="1">' +
                    '<mxGeometry x="186.5" y="388" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="105" value="interfaz3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF3_N3" vertex="1">' +
                    '<mxGeometry x="37" y="300" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="106" value="interfaz4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF4_N4" vertex="1">' +
                    '<mxGeometry x="149" y="204" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="107" value="interfaz5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF5_N5" vertex="1">' +
                    '<mxGeometry x="277" y="123" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="108" value="interfaz6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF6_N6" vertex="1">' +
                    '<mxGeometry x="395.5" y="43" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="109" value="interfaz7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF7_N7" vertex="1">' +
                    '<mxGeometry x="536" y="24" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="110" value="interfaz8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF8_N8" vertex="1">' +
                    '<mxGeometry x="727.0000000000001" y="37" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="111" value="interfaz9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF9_N9" vertex="1">' +
                    '<mxGeometry x="900" y="98.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="112" value="interfaz10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF10_N10" vertex="1">' +
                    '<mxGeometry x="881.5" y="272" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="171" value="Ip1" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP1" vertex="1">' +
                    '<mxGeometry x="372" y="519" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="172" value="Ip2" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP2" vertex="1">' +
                    '<mxGeometry x="186" y="466" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="173" value="Ip3" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP3" vertex="1">' +
                    '<mxGeometry x="40" y="379" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="174" value="Ip4" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP4" vertex="1">' +
                    '<mxGeometry x="162" y="279" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="175" value="Ip5" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP5" vertex="1">' +
                    '<mxGeometry x="279" y="203" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="176" value="Ip6" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP6" vertex="1">' +
                    '<mxGeometry x="399.5" y="122" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="177" value="Ip7" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP7" vertex="1">' +
                    '<mxGeometry x="551" y="106" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="178" value="Ip8" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP8" vertex="1">' +
                    '<mxGeometry x="731.5" y="124" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="179" value="Ip9" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP9" vertex="1">' +
                    '<mxGeometry x="902" y="177" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="180" value="Ip10" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP10" vertex="1">' +
                    '<mxGeometry x="890" y="350" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="78" value="nodo 2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-9;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo2" vertex="1">' +
                    '<mxGeometry x="209" y="401" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="77" value="nodo 3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-19;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo3" vertex="1">' +
                    '<mxGeometry x="60" y="313" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="76" value="nodo 4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo4" vertex="1">' +
                    '<mxGeometry x="177" y="215" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="70" value="nodo 5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo5" vertex="1">' +
                    '<mxGeometry x="297" y="137.5" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="72" value="nodo 7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-10;spacingLeft=-27;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo7" vertex="1">' +
                    '<mxGeometry x="569" y="36" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="183" value="DOWN1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN1" vertex="1">' +
                    '<mxGeometry x="407" y="549" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="182" value="DOWN2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-43;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN2" vertex="1">' +
                    '<mxGeometry x="221.5" y="498" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="181" value="DOWN3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN3" vertex="1">' +
                    '<mxGeometry x="71" y="415" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="185" value="DOWN4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-48;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN4" vertex="1">' +
                    '<mxGeometry x="193" y="311" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="186" value="DOWN5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-53;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN5" vertex="1">' +
                    '<mxGeometry x="311" y="241" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="187" value="DOWN6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-47;spacingRight=0;spacingLeft=2;fontStyle=1;fontSize=15;spacingBottom=-6;" parent="1" class="DOWN DOWN6" vertex="1">' +
                    '<mxGeometry x="437" y="151" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="189" value="DOWN7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=4;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN7" vertex="1">' +
                    '<mxGeometry x="581" y="139" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="190" value="DOWN8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-44;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN8" vertex="1">' +
                    '<mxGeometry x="765" y="159" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="188" value="DOWN9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN9" vertex="1">' +
                    '<mxGeometry x="941" y="208" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="184" value="DOWN10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN10" vertex="1">' +
                    '<mxGeometry x="926" y="378.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="27" value="BANDA2" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA2" vertex="1">' +
                    '<mxGeometry x="154" y="403" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="26" value="BANDA3" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA3" vertex="1">' +
                    '<mxGeometry x="6" y="314" width="50" height="53" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="28" value="BANDA4" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA4" vertex="1">' +
                    '<mxGeometry x="116" y="205" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="25" value="BANDA5" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA5" vertex="1">' +
                    '<mxGeometry x="239" y="128" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="24" value="BANDA6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA6" vertex="1">' +
                    '<mxGeometry x="365" y="55" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="23" value="BANDA7" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA7" vertex="1">' +
                    '<mxGeometry x="632" y="28.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="21" value="BANDA9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA9" vertex="1">' +
                    '<mxGeometry x="980" y="115.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="89" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=0;exitY=0.5;exitDx=0;exitDy=0;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="75" target="73" edge="1" class="txtlineaConec txtlineaConec_n8-n9">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="735" y="330.1362432702593" as="sourcePoint" />' +
                    '<mxPoint x="780" y="100" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="870" y="110" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="100" value="N8-N9" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="89" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="0.2033" y="-2" relative="1" as="geometry">' +
                    '<mxPoint x="26" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="87" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=0.75;exitY=0;exitDx=0;exitDy=0;entryX=0;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="194" target="74" edge="1" class="txtlineaConec txtlineaConec_n10-n11">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="369" y="322" as="sourcePoint" />' +
                    '<mxPoint x="580" y="311.3771324027148" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="102" value="N10-N11" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="87" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="41.40000000000003" y="-19.10000000000002" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="191" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="194" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="758" y="291.5" as="sourcePoint" />' +
                    '<mxPoint x="843" y="440.5" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="192" value="interfaz11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF11_N11" vertex="1">' +
                    '<mxGeometry x="745.5" y="351.25" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="193" value="Ip11" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP11" vertex="1">' +
                    '<mxGeometry x="749" y="433.5" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="195" value="DOWN11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN11" vertex="1">' +
                    '<mxGeometry x="785" y="463" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="196" value="BANDA11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA11" vertex="1">' +
                    '<mxGeometry x="829" y="376" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="73" value="nodo 8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-40;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo8" vertex="1">' +
                    '<mxGeometry x="754" y="57" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="22" value="BANDA8" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA8" vertex="1">' +
                    '<mxGeometry x="810" y="30" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="198" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="1" source="79" edge="1" class="txtlineaConec txtlineaConec_n12-n1" target="217">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="422" y="419.2428675972852" as="sourcePoint" />' +
                    '<mxPoint x="490" y="540" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="450" y="510" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="199" value="N13-N1" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="198" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="51" y="15.4" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="20" value="BANDA10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA10" vertex="1">' +
                    '<mxGeometry x="967" y="287.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="29" value="BANDA1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;spacingBottom=-8;" parent="1" class="BANDA BANDA1" vertex="1">' +
                    '<mxGeometry x="332.5" y="467" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="71" value="nodo 6" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-11;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo6" vertex="1">' +
                    '<mxGeometry x="422" y="57" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="202" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;exitX=1;exitY=0.75;exitDx=0;exitDy=0;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" edge="1" class="txtlineaConec txtlineaConec_n11-n12" target="194" source="207">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="425.5" y="370.5028675972852" as="sourcePoint" />' +
                    '<mxPoint x="540" y="310" as="targetPoint" />' +
                    '<Array as="points" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="203" value="N11-N12" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="202" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="30.090000000000007" y="19.289999999999992" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="204" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="207" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="601.5" y="181.25" as="sourcePoint" />' +
                    '<mxPoint x="686.5" y="330.25" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="205" value="interfaz12" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF12_N12" vertex="1">' +
                    '<mxGeometry x="590" y="244" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="206" value="Ip12" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP12" vertex="1">' +
                    '<mxGeometry x="588.5" y="325.25" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="208" value="DOWN12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN12" vertex="1">' +
                    '<mxGeometry x="628.5" y="354.75" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="209" value="BANDA12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA12" vertex="1">' +
                    '<mxGeometry x="544" y="259.25" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="212" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;entryX=0;entryY=0.75;entryDx=0;entryDy=0;exitX=0.25;exitY=0;exitDx=0;exitDy=0;" parent="1" source="217" target="207" edge="1" class="txtlineaConec txtlineaConec_n12-n13">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="645" y="531.3728675972852" as="sourcePoint" />' +
                    '<mxPoint x="703" y="497" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="510" y="410" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="213" value="N12-N13" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="212" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4594" y="-3" relative="1" as="geometry">' +
                    '<mxPoint x="-54.119999999999976" y="-19.019999999999982" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="214" value="o10" style="rounded=0;comic=1;strokeWidth=3;endArrow=none;html=1;fontFamily=Comic Sans MS;fontStyle=1;endFill=0;strokeColor=#E6E6E6;entryX=0;entryY=0;entryDx=0;entryDy=0;" parent="1" source="217" class="LNOLT LNOLT3" edge="1">' +
                    '<mxGeometry width="50" height="50" relative="1" as="geometry">' +
                    '<mxPoint x="572" y="379" as="sourcePoint" />' +
                    '<mxPoint x="657" y="528" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="215" value="interfaz13" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#0000FF;" parent="1" class="ITF ITF13_N13" vertex="1">' +
                    '<mxGeometry x="564.5" y="442" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="216" value="Ip13" style="text;strokeColor=none;fillColor=none;html=1;fontSize=13;fontStyle=0;verticalAlign=middle;align=center;fontColor=#000000;" parent="1" class="IP IP13" vertex="1">' +
                    '<mxGeometry x="565" y="520" width="100" height="20" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="217" value="nodo 13" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo13" vertex="1">' +
                    '<mxGeometry x="582" y="457" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="218" value="DOWN13" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/OLT.png;spacingTop=-41;spacingRight=0;spacingLeft=6;fontStyle=1;fontSize=15;" parent="1" class="DOWN DOWN13" vertex="1">' +
                    '<mxGeometry x="599" y="550.5" width="30" height="30" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="219" value="BANDA13" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/50.png;spacingTop=7;spacingRight=0;spacingLeft=5;fontStyle=1;fontSize=15;" parent="1" class="BANDA BANDA13" vertex="1">' +
                    '<mxGeometry x="649" y="463.5" width="50" height="50" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="74" value="nodo 10" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo10" vertex="1">' +
                    '<mxGeometry x="909" y="285" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="75" value="nodo 9" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo9" vertex="1">' +
                    '<mxGeometry x="922" y="113" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="82" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;entryX=0;entryY=0.5;entryDx=0;entryDy=0;strokeWidth=3;strokeColor=#E6E6E6;shadow=1;" parent="1" target="76" edge="1" class="txtlineaConec txtlineaConec_n3-n4">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="83" y="300" as="sourcePoint" />' +
                    '<mxPoint x="105" y="155" as="targetPoint" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="95" value="N3-N4" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="82" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.365" relative="1" as="geometry">' +
                    '<mxPoint x="4.959999999999994" y="0.5" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="88" value="" style="edgeStyle=isometricEdgeStyle;endArrow=none;html=1;fontSize=15;strokeColor=#E6E6E6;strokeWidth=3;shadow=1;exitX=0;exitY=0;exitDx=0;exitDy=0;entryX=0.25;entryY=1;entryDx=0;entryDy=0;" parent="1" source="74" target="75" edge="1" class="txtlineaConec txtlineaConec_n9-n10">' +
                    '<mxGeometry width="50" height="100" relative="1" as="geometry">' +
                    '<mxPoint x="990" y="740" as="sourcePoint" />' +
                    '<mxPoint x="998.83" y="449.9971324027148" as="targetPoint" />' +
                    '<Array as="points">' +
                    '<mxPoint x="870" y="260" />' +
                    '</Array>' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="101" value="N9-N10" style="text;html=1;align=center;verticalAlign=middle;resizable=0;points=[];labelBackgroundColor=#ffffff;fontSize=12;" parent="88" vertex="1" connectable="0" class="linea">' +
                    '<mxGeometry x="-0.4187" relative="1" as="geometry">' +
                    '<mxPoint x="15.589999999999996" y="-36" as="offset" />' +
                    '</mxGeometry>' +
                    '</mxCell>' +
                    '<mxCell id="194" value="nodo 11" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo11" vertex="1">' +
                    '<mxGeometry x="768" y="369.5" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="207" value="nodo 12" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo12" vertex="1">' +
                    '<mxGeometry x="611.5" y="259.25" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '<mxCell id="79" value="nodo 1" style="shape=image;html=1;labelBackgroundColor=#ffffff;image=/edt/stencils/clipart/switch.png;spacingRight=-30;spacingBottom=-80;spacingTop=-12;spacingLeft=-11;fontStyle=1;fontSize=13;fontColor=#1A1A1A;" parent="1" class="nodo nodo1" vertex="1">' +
                    '<mxGeometry x="388" y="454" width="55" height="55" as="geometry" />' +
                    '</mxCell>' +
                    '</root>' +
                    '</mxGraphModel>'
            }
        ],
        anillos: [],
        anillosE: [],
        btnActivo: false,
        modoEdicion: false,
        modoNuevoNodo: false,
        modoNuevoAnillo: false,
        deshabilitadoPorRunning: false,
        editedNodo: {
            valNombreNodo: '',
            valIpEquipo: '',
            valMPLS: '',
            valVrrp: '',
            valOrden: '',
            valInterfaz: '',
            valDown: null,
            valLLDP: null,
            valConectaCon: '',
            valIpColector: '',
            valIpLANWAN: '',
            valipolt: '',
            valposont: '',
            valIpDestino: '',
            valIpDestino2: '',
            editedIndex: 0,
            modelo: '',
            version: ''
        },
        nombreAnillo: '',
        nodosAnillo: [],
        indexAnilloEdit: -1,
        menuAnilloSeleccionado: null,
        menuAnilloSeleccionadoE: null,
        //anilloEstatatusDeshabilitado:false,
        validacionAcceso: {
            anillos: {
                anillo: '',
                bloqueo: 1,
                equipos: []
            },
            usuario: '',
            fechahora: '',
            servicios: {
                userlw: '',
                pwdlw: '',
                iplw: '',
                anillo: '',
                datos: [],
                internet_voz: []
            },
            updownlist: []
        },
        macroProceso: {
            valInicial: false, //llenado 864
            ejecucion: {}, //llenado 865
            mustReset: false,
            vecesDown: 0, //incrementado en
            vueltasEjecucion: 0,
            arrEjecucionesDown: [], //llenado ln 591
            arrDownListData: [],
            final: false,
            next: '',
            equipoDown: null,
            equipoDownLevantadoOK: false,
            equipoDownRetomado: false
        },
        REQPROXY: {},
        ejecucion: {
            valAccesosProcess: false,
            valAccesosPermisos: false,
            messageIniciaValidacion: 0,
            incrementoIndicadorGeneral: 0,
            indicadorGeneral: 0,
            estaSeguro: false,
            processID: '',
            processLLDPID: '',
            time: '00:00:00',
            diff: '',
            inicio: '',
            actual: '',
            vuelta: '',
            timerID: '',
            fechaInicio: new Date().getTime(),
            fechaFin: new Date().getTime(),
            loaded: false,
            isRunning: false,
            //Paul
            isRunningValPing: false,
            //
            stepProceso: -1,
            logEjecucion: [],
            idLog: 0,
            datosColor: 'grey',
            accesos2Color: 'white',
            accesosColor: 'white',
            permisosColor: 'white',
            permisos2Color: 'white',
            serviciosColor: 'grey',
            protocolosColor: 'grey',
            evidenciasColor: 'grey',
            continua: true,
            seguridad: {
                rfc: '',
                userEquiposAnillo: '',
                pwdEquiposAnillo: '',
                userColector: '',
                pwdColector: '',
                userLanwan: '',
                pwdLanwan: '',
                userOlt: '',
                pwdOlt: '',
                userUrlVideo: '',
                pwdUrlVideo: ''
            },
            valInicialAvance: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            valDownAvance: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            valUpAvance: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            stepsEjecucion: [
                { id: 1, step: 'accesos', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 2, step: 'pingLanWan', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 3, step: 'ospf', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 4, step: 'bgp', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 5, step: 'bgpAllSumm', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },

                { id: 6, step: 'voz', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 7, step: 'internet', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                // { id:7 , step:'video'        ,salida:'',procesando:false,fechaIni:'',fechaFin:'',color:'grey',imagen:'',continua:true,nodos:[], log:[]},
                { id: 8, step: 'mpls', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 9, step: 'interfacesLld', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },

                { id: 10, step: 'vrrp', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 11, step: 'evidencias', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
            ],
            stepsEjecucionX: [
                { id: 1, step: 'pingLanWan', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 2, step: 'ospf', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 3, step: 'bgp', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 4, step: 'bgpAllSumm', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },

                { id: 5, step: 'voz', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 6, step: 'internet', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 7, step: 'mpls', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 8, step: 'interfacesLld', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },

                { id: 9, step: 'vrrp', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },
                { id: 10, step: 'evidencias', salida: '', procesando: false, fechaIni: '', fechaFin: '', color: 'white', imagen: '', continua: true, nodos: [], log: [] },

            ],
            lstStatusAnillos: [
                { estatus: 'EDICIÓN', id: 1 },
                { estatus: 'VALIDADO', id: 2 },
                { estatus: 'EN EJECUCION', id: 3 },
                { estatus: 'DESHABILITADO', id: 4 },
            ],
        },
        stepsEjecucion: [
            { step: 'pingLanWan', id: 1, habilitado: true },
            { step: 'ospf', id: 2, habilitado: true },
            { step: 'bgp', id: 3, habilitado: true },
            { step: 'bgpAllSumm', id: 4, habilitado: true },
            { step: 'voz', id: 5, habilitado: true },
            { step: 'internet', id: 6, habilitado: true },
            //{step:'video',id: 7,habilitado:false},
            { step: 'mpls', id: 8, habilitado: true },
            { step: 'interfacesLldp', id: 9, habilitado: true },
            { step: 'vrrp', id: 10, habilitado: true },
            { step: 'evidencias', id: 11, habilitado: true },
        ],
        stepsEjecucionX: [
            { step: 'pingLanWan', id: 1, habilitado: true },
            { step: 'ospf', id: 2, habilitado: true },
            { step: 'bgp', id: 3, habilitado: true },
            { step: 'bgpAllSumm', id: 4, habilitado: true },
            { step: 'voz', id: 5, habilitado: true },
            { step: 'internet', id: 6, habilitado: true },
            //{step:'video',id: 7,habilitado:false},
            { step: 'mpls', id: 8, habilitado: true },
            { step: 'interfacesLldp', id: 9, habilitado: true },
            { step: 'vrrp', id: 10, habilitado: true },
            { step: 'evidencias', id: 11, habilitado: true },
        ],
        arrequipos: []
    },
    getters: {
        logs: state => {

            if (state.ejecucion.logEjecucion == null) {
                state.ejecucion.logEjecucion = [];
                state.ejecucion.idLog = state.ejecucion.idLog + 1;
                state.ejecucion.logEjecucion.push({ id: state.ejecucion.idLog, log: this.getDate() + ' ' + mensaje });
            }
            return state.ejecucion.logEjecucion;
        },
    },
    mutations: {},
    actions: {},
    modules: {}
})