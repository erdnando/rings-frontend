import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import NuevoAnillo from '../views/NuevoAnillo.vue'
import Anillos from '../views/Anillos.vue'
import Ejecuciona from '../views/Ejecucion-a.vue'
import Ejecucionb from '../views/Ejecucion-b.vue'
import EjecucionDown from '../views/Ejecucion-down.vue'
import EjecucionUp from '../views/Ejecucion-up.vue'
import EjecucionFinal from '../views/Ejecucion-final.vue'
import EjecucionBloqueada from '../views/Ejecucion-bloqueada.vue'
import Roles from '../views/Roles.vue'
import Historial from '../views/Historial.vue'
import Parametros from '../views/Parametros.vue'


Vue.use(VueRouter);
Vue.prototype.$eventHub = new Vue();

const routes = [{
        path: '/',
        name: 'home',
        component: Home
    },
    {
        path: '/about',
        name: 'about',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import ( /* webpackChunkName: "about" */ '../views/About.vue')
    },
    {
        path: '/nuevo-anillo',
        name: 'nuevo-anillo',
        component: NuevoAnillo
    },
    {
        path: '/anillos',
        name: 'anillos',
        component: Anillos
    },
    {
        path: '/ejecucion-a',
        name: 'ejecucion-a',
        component: Ejecuciona
    },
    {
        path: '/ejecucion-b',
        name: 'ejecucion-b',
        component: Ejecucionb
    },
    {
        path: '/ejecucion-down',
        name: 'ejecucion-down',
        component: EjecucionDown
    },
    {
        path: '/ejecucion-up',
        name: 'ejecucion-up',
        component: EjecucionUp
    },
    {
        path: '/ejecucion-final',
        name: 'ejecucion-final',
        component: EjecucionFinal
    },
    {
        path: '/ejecucion-bloqueada',
        name: 'ejecucion-bloqueada',
        component: EjecucionBloqueada
    },
    {
        path: '/historial',
        name: 'historial',
        component: Historial
    },
    {
        path: '/parametros',
        name: 'parametros',
        component: Parametros
    },
    {
        path: '/roles',
        name: 'roles',
        component: Roles
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router